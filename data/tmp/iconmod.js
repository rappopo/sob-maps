const path = require('path')

module.exports = function (f, opts) {
  const { _ } = opts
  const ext = path.extname(f)
  const base = path.basename(f, ext)
  return {
    id: f,
    name: _.startCase(base)
  }
}
