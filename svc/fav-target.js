const { _, Sequelize } = require('rappopo-sob').Helper
const shared = _.map(require('rappopo-sob/data/sd/shared.json'), 'id')

const schema = {
  siteId: Sequelize.INTEGER,
  userId: Sequelize.INTEGER,
  shared: { type: Sequelize.STRING(2), defaultValue: 'NO' },
  category: { type: Sequelize.STRING(30), defaultValue: 'Uncategorized' },
  name: Sequelize.STRING(50),
  tid: Sequelize.STRING(20),
  type: Sequelize.STRING(10),
  note: Sequelize.TEXT
}

const fields = _.concat(
  ['id', 'createdAt', 'updatedAt'],
  _.keys(schema)
)

const entityValidator = {
  name: { type: 'string', empty: false, trim: true },
  shared: { type: 'enum', values: shared },
  tid: { type: 'string', empty: false },
  type: { type: 'string', empty: false }
}

module.exports = ({ dbSql }) => {
  return {
    mixins: [dbSql],
    model: {
      define: schema,
      options: {
        underscored: true,
        indexes: [
          { unique: true, fields: ['tid', 'type', 'name', 'user_id', 'site_id'] },
          { fields: ['shared'] },
          { fields: ['category'] },
          { fields: ['created_at'] },
          { fields: ['updated_at'] }
        ]
      }
    },
    settings: {
      fields,
      entityValidator
    }
  }
}
