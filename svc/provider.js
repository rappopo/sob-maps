const { _, Sequelize } = require('rappopo-sob').Helper

const schema = {
  siteId: Sequelize.INTEGER,
  code: { type: Sequelize.STRING(30), unique: true },
  name: Sequelize.STRING(50),
  subDomain: Sequelize.STRING(10),
  type: { type: Sequelize.STRING(10) },
  provider: Sequelize.STRING(20),
  url: Sequelize.STRING,
  tms: Sequelize.BOOLEAN,
  allowProxy: Sequelize.BOOLEAN,
  minZoom: { type: Sequelize.INTEGER, defaultValue: 0 },
  maxZoom: { type: Sequelize.INTEGER, defaultValue: 0 },
  overlay: { type: Sequelize.BOOLEAN, defaultValue: false },
  attribution: Sequelize.TEXT,
  builtIn: { type: Sequelize.BOOLEAN, defaultValue: false },
  active: { type: Sequelize.BOOLEAN, defaultValue: true },
  data: Sequelize.TEXT
}

const fields = _.concat(
  ['id', 'createdAt', 'updatedAt'],
  _.keys(schema)
)

const entityValidator = {
  code: { type: 'string', empty: false, trim: true, lowercase: true },
  name: { type: 'string', empty: false, trim: true },
  type: { type: 'string', empty: false, trim: true },
  url: { type: 'string', empty: false, trim: true, lowercase: true },
  minZoom: { type: 'number', min: 0, max: 20, integer: true },
  maxZoom: { type: 'number', min: 0, max: 20, integer: true }
}

module.exports = ({ dbSql }) => {
  return {
    mixins: [dbSql],
    model: {
      define: schema,
      options: {
        underscored: true,
        indexes: [
          { fields: ['type'] },
          { fields: ['created_at'] },
          { fields: ['updated_at'] }
        ]
      }
    },
    settings: {
      webApi: { readonly: true, anonymous: ['list', 'get'] },
      fields,
      entityValidator
    }
  }
}
