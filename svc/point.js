const { _, Sequelize } = require('rappopo-sob').Helper
const shared = _.map(require('rappopo-sob/data/sd/shared.json'), 'id')

const schema = {
  siteId: Sequelize.INTEGER,
  userId: Sequelize.INTEGER,
  shared: { type: Sequelize.STRING(2), defaultValue: 'NO' },
  category: { type: Sequelize.STRING(30), defaultValue: 'Uncategorized' },
  name: Sequelize.STRING(50),
  icon: { type: Sequelize.STRING(50), defaultValue: 'default' },
  iconSet: { type: Sequelize.STRING(20), defaultValue: 'default' },
  lat: Sequelize.DOUBLE,
  lng: Sequelize.DOUBLE,
  buffer: { type: Sequelize.DOUBLE, defaultValue: 0 },
  active: { type: Sequelize.BOOLEAN, defaultValue: true },
  note: Sequelize.TEXT
}

const fields = _.concat(
  ['id', 'createdAt', 'updatedAt'],
  _.keys(schema)
)

const entityValidator = {
  name: { type: 'string', empty: false, trim: true },
  shared: { type: 'enum', values: shared },
  lat: { type: 'number', min: -90, max: 90 },
  lng: { type: 'number', min: -180, max: 180 }
}

module.exports = ({ dbSql }) => {
  return {
    mixins: [dbSql],
    model: {
      define: schema,
      options: {
        underscored: true,
        indexes: [
          { fields: ['category'] },
          { fields: ['name'] },
          { fields: ['lat'] },
          { fields: ['lng'] },
          { fields: ['active'] },
          { fields: ['site_id'] },
          { fields: ['user_id'] },
          { fields: ['shared'] },
          { fields: ['created_at'] },
          { fields: ['updated_at'] }
        ]
      }
    },
    settings: {
      fields,
      entityValidator
    }
  }
}
