const { _, Sequelize } = require('rappopo-sob').Helper
const shared = _.map(require('rappopo-sob/data/sd/shared.json'), 'id')

const schema = {
  siteId: Sequelize.INTEGER,
  userId: Sequelize.INTEGER,
  shared: { type: Sequelize.STRING(2), defaultValue: 'NO' },
  category: { type: Sequelize.STRING(30), defaultValue: 'Uncategorized' },
  name: Sequelize.STRING(50),
  bColor: { type: Sequelize.STRING(10), defaultValue: '#000000' },
  bSize: { type: Sequelize.INTEGER, defaultValue: 2 },
  opacity: { type: Sequelize.DOUBLE, defaultValue: 0.7 },
  buffer: { type: Sequelize.DOUBLE, defaultValue: 0 },
  active: { type: Sequelize.BOOLEAN, defaultValue: true },
  wkt: Sequelize.TEXT,
  note: Sequelize.TEXT
}

const fields = _.concat(
  ['id', 'createdAt', 'updatedAt'],
  _.keys(schema)
)

const entityValidator = {
  name: { type: 'string', empty: false, trim: true },
  shared: { type: 'enum', values: shared },
  bSize: { type: 'number', min: 0, integer: true },
  opacity: { type: 'number', min: 0, max: 1 },
  wkt: { type: 'string', empty: false, trim: true }
}

module.exports = ({ dbSql }) => {
  return {
    mixins: [dbSql],
    model: {
      define: schema,
      options: {
        underscored: true,
        indexes: [
          { fields: ['category'] },
          { fields: ['name'] },
          { fields: ['active'] },
          { fields: ['site_id'] },
          { fields: ['user_id'] },
          { fields: ['shared'] },
          { fields: ['created_at'] },
          { fields: ['updated_at'] }
        ]
      }
    },
    settings: {
      fields,
      entityValidator
    }
  }
}
